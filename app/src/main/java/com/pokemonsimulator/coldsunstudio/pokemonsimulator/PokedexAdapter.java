package com.pokemonsimulator.coldsunstudio.pokemonsimulator;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class PokedexAdapter extends BaseAdapter {

    private Context mContext;
    private List<Pokemon> mPokemonList;

    public PokedexAdapter(Context context, List<Pokemon> pokemonList) {
        mContext = context;
        mPokemonList = pokemonList;
    }

    @Override
    public int getCount() {
        return mPokemonList.size();
    }

    @Override
    public Object getItem(int i) {
        return mPokemonList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return mPokemonList.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LinearLayout layout = new LinearLayout(mContext);
        TextView t1 = new TextView(mContext);
        t1.setText(String.valueOf(mPokemonList.get(i).getId()));
        TextView t2 = new TextView(mContext);
        t2.setText(mPokemonList.get(i).getName());
        layout.addView(t1);
        layout.addView(t2);
        return layout;
    }
}
