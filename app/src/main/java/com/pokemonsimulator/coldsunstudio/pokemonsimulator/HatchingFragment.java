package com.pokemonsimulator.coldsunstudio.pokemonsimulator;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.*;
import butterknife.*;

public class HatchingFragment extends Fragment {

    private Unbinder unbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_hatching, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}