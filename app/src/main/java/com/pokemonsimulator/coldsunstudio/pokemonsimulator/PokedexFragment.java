package com.pokemonsimulator.coldsunstudio.pokemonsimulator;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class PokedexFragment extends Fragment {

    @BindView(R.id.pokedex_grid)
    GridView mPokedexGridView;
    private Unbinder mUnbinder;
    private List<Pokemon> mPokemons;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pokedex, container, false);
        mUnbinder = ButterKnife.bind(this, rootView);
        // @TODO get pokemons
        mPokemons = getPokemons();

        mPokedexGridView.setAdapter(new PokedexAdapter(getContext(), mPokemons));
        return rootView;
    }

    //@TODO delete:
    private List<Pokemon> getPokemons() {
        List<Pokemon> list = new ArrayList<>();
        for(int i = 0; i < 10; i++)
            list.add(new Pokemon(i, "Pokemon" + i));
        return list;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

}
